package com.supcom.recipeQuest.registration;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path= "api/registration")
@AllArgsConstructor
@CrossOrigin("http://localhost:4200/")
public class RegistrationController {

    private RegistrationService registrationService;

    @PostMapping
    public ResponseEntity<?> register(@RequestBody RegistrationRequest request){
        return registrationService.register(request);
    }
}
