package com.supcom.recipeQuest.login;


import com.supcom.recipeQuest.appuser.AppUser;
import com.supcom.recipeQuest.appuser.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/login")
@AllArgsConstructor
@CrossOrigin("http://localhost:4200/")
public class LoginController{

    @Autowired
    private final UserRepository repo;

    @Autowired
    private final BCryptPasswordEncoder bCryptPasswordEncoder;


    @PostMapping
    public ResponseEntity<?> login(@RequestBody AppUser userData){
        AppUser user = repo.findByEmail(userData.getEmail()).get();
        if(user.getPassword().equals(userData.getPassword())){
            return ResponseEntity.ok(user);
        }
        return (ResponseEntity<?>) ResponseEntity.internalServerError();
    }
}

