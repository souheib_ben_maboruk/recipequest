package com.supcom.recipeQuest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@SpringBootApplication
@RestController
public class RecipeQuestApplication {
	public static void main(String[] args) {
		SpringApplication.run(RecipeQuestApplication.class, args);
		//https://api.spoonacular.com/recipes/findByIngredients?apiKey=8a7d17fa9581449e9600bd8457c07f9a&ingredients=apples,+flour,+sugar&number=5

	}
	@GetMapping
	public String hello(){
		return "Hello world";
	}
}
