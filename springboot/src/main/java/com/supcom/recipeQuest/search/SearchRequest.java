package com.supcom.recipeQuest.search;


import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class SearchRequest {
    private String apiKey;
    private String ingredients;
}
