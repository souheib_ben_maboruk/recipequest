package com.supcom.recipeQuest.search;


import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping(path="api/search")
@AllArgsConstructor
public class SearchController {
    private SearchService searchService;

    @PostMapping
    public String search() throws IOException {
        return searchService.search();
    }
}
