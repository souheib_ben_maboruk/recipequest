package com.supcom.recipeQuest.search;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

@Service
@AllArgsConstructor
public class SearchService {
    public String search() throws IOException {
        URL url = new URL("https://google.com");
        try{
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setDoOutput(true);
            DataOutputStream out = new DataOutputStream(con.getOutputStream());
            out.flush();
            out.close();
            return out.toString();
        }catch(Exception e){
        }
        return "Problem";
    }
}
