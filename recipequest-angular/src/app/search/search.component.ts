import { Component, OnInit } from '@angular/core';
import { Ingredients } from '../ingredients';
import {Router} from '@angular/router';




@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {


  message:  string = "";
  ingredient: string = "";
  Ingredients: Array<string> = [];
  db64:any;

  travel(){
    this.db64 = btoa(JSON.stringify(this.Ingredients));
    this.route.navigate(['/results', { data:this.db64  }]);
  }

  constructor(private route:Router) {}

  ngOnInit(): void {
  }

  addElement(){
    if(this.ingredient != ""){
      this.message = "";
      this.Ingredients.push(this.ingredient);
      this.ingredient = "";
    }else{
      this.message = "Please Insert an ingredient"; 
    }
  }

}
