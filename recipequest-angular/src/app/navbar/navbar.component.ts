import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  cookieData:any;
  dataBool:Boolean=false;

  constructor(private cookieService:CookieService) { }
  logout(){
    this.cookieService.delete('log');
  }

  ngOnInit(): void {
    if(this.cookieService.check('log')){
      this.cookieData = JSON.parse(atob(this.cookieService.get('log')));
      this.dataBool = true;
    }else{
      console.log("nothging");
    }
  }

}
