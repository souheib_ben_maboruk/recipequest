import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import { UserSign } from '../user-sign';



@Injectable({
  providedIn: 'root'
})
export class SignupService {
  url = "http://127.0.0.1:8080/api/registration"

  constructor(private http:HttpClient) { }

  signUp(user:UserSign){
    console.log(user);
    return this.http.post(this.url, user);
  }
}
