import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import { User } from '../user';


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  url = "http://127.0.0.1:8080/api/login"

  constructor(private http:HttpClient) { }

  login(user:User){
    console.log(user);
    return this.http.post(this.url, user);
  }

}
