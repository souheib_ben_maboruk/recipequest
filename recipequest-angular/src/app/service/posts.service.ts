import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  ingredients:string="";
  url = "https://api.spoonacular.com/recipes/findByIngredients?apiKey=8a7d17fa9581449e9600bd8457c07f9a&ingredients=";
  constructor(private http:HttpClient) { }
  addIngredients(data:string){
    this.ingredients = data;
  }
  getPosts(){
    return this.http.get(this.url+this.ingredients+"&number=10");
  }
}
