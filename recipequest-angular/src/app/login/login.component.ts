import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { LoginService } from '../auth/login.service';
import { CookieService } from 'ngx-cookie-service';
import {  Router } from '@angular/router';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private cookieValue:string="";

  user:User = new User();
  data:any;
  email:string="";
  password: string="";
  errorB:boolean=false;

  hello(){
    this.user.email = this.email;
    this.user.password = this.password;
    this.loginService.login(this.user).subscribe(data=>{
    this.data = data;
    this.cookieValue = btoa(JSON.stringify(this.data));
    this.cookieService.set('log', this.cookieValue);
    this.route.navigate(['/'])
    .then(() => {
      window.location.reload();
    });          
    },error=>{
      this.errorB=true;
    })
    console.log(this.data);
  }

  constructor(private loginService:LoginService,private route:Router, private cookieService: CookieService) { }

  ngOnInit(): void {
    if(this.cookieService.check('log'))
    this.route.navigate(['/']); // navigate to other page
  }

}
