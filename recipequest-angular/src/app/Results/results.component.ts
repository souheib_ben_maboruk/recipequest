import { Component, OnInit } from '@angular/core';
import { PostsService } from '../service/posts.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {
  data:any;
  data1:any;
  db64: string="";
  buffer:string="";
  response:any;
  private sub:any;
  
  travel(i:number){
    console.log(this.data[i]);
    this.db64 = btoa(JSON.stringify(this.data[i]));
    console.log(this.data[i]);
    this.aroute.navigate(['/recipe', { data:this.db64  }]);
  }

  constructor(private postData:PostsService, private aroute:Router, private route: ActivatedRoute){}

  ngOnInit(){
      this.sub = this.route.params.subscribe(params => {
      this.response = params['data'];
      this.data1= JSON.parse(atob(this.response));
       });
      for (var ingredient of this.data1) {
        this.buffer = this.buffer +ingredient+ ",+";
      }
      console.log(this.buffer);
    this.postData.addIngredients(this.buffer);
    this.postData.getPosts().subscribe((result)=>{
    this.data = result;
    })
  }

}
