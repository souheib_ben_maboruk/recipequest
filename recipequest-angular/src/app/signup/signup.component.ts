import { Component, OnInit } from '@angular/core';
import { UserSign } from '../user-sign';
import { SignupService } from '../auth/signup.service';
import { CookieService } from 'ngx-cookie-service';
import {  Router } from '@angular/router';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  user:UserSign = new UserSign();
  errorB:boolean = false;
  message:string ="";

  signup(){
    console.log(this.user);
    if(!this.user.email || !this.user.password || !this.user.firstName || !this.user.lastName){
      this.errorB=true;
      this.message = "No Empty form pls";
      return ;
    }
    this.errorB=false;
    this.signupService.signUp(this.user).subscribe(data=>{
      alert('Success');
      this.route.navigate(['/login']);
    },error=>{
      this.errorB=true;
      this.message = "Unknown Error"
    })
  }

  constructor(private cookieService:CookieService, private signupService:SignupService, private route:Router) { }

  ngOnInit(): void {
    if(this.cookieService.check('log'))
    this.route.navigate(['/']); // navigate to other page
  }

}
