import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.css']
})
export class RecipeComponent implements OnInit {
  title: string="hamma chrab";
  response: string="";
  img: string="https://spoonacular.com/recipeImages/640352-312x231.jpg";
  id: number=0;
  data:any;
  private sub: any;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
       this.response = params['data'];
       this.data= JSON.parse(atob(this.response));
       });
  }

}
